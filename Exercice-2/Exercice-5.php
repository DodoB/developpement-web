<!doctype html>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Formualaire</title>
  </head>
  <body>
   
    <div class="container">
        <div class="row" style="margin-top: 20%;">
            <div class="col-lg-12" style="background-color: #F7F7F7; border-radius: 20px;">
                <form class="mt-3" style="background-color: transparent;" action="http://univcergy.phpnet.org/CCI/animal.php" method="POST">

                    <div class="form-group">
                      <label for="login">Login </label>
                      <input type="text" name="identifiant" class="form-control" id="identifiant" placeholder="entrez votre login">
                    </div>

                    <div class="form-group">
                        <label for="password">password </label>
                        <input type="password" name="mdp" class="form-control" id="password" placeholder="entrez votre mot de passe ">
                      </div>

                    <div class="form-group">
                        <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">
                            <label class="custom-control-label" for="customRadioInline1">Enseignement </label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input">
                            <label class="custom-control-label" for="customRadioInline2">Etudiant</label>
                          </div>
                    </div>

                    <button type="submit" class="btn btn-success btn-lg mt-3 mb-3" >Chercher</button>
                  </form>
            </div>
        </div>
    </div>

      <!-- On appelle la variable par le name 
            //$identifiant = $_POST["identifiant"];
            //$password = $_POST["password"];
            
            /*try{
                //On se connecte à la BDD
               //$dbco = new PDO("mysql:host=$serveur;dbname=$dbname",$user,$pass);
                $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
                //On insère les données reçues si les champs sont remplis
                if(!empty($prenom)  && !empty($mail) && !empty($age)){
                    $sth = $dbco->prepare("
                        INSERT INTO form(prenom, mail, age)
                        VALUES(:prenom, :mail, :age)");
                    $sth->bindParam(':prenom',$prenom);
                    $sth->bindParam(':mail',$mail);
                    $sth->bindParam(':age',$age);
                    $sth->execute();
                }
                
                //On récupère les infos de la table 
                $sth = $dbco->prepare("SELECT prenom, mail, age FROM form");
                $sth->execute();
                //On affiche les infos de la table
                $resultat = $sth->fetchAll(PDO::FETCH_ASSOC);
                $keys = array_keys($resultat);
                for($i = 0; $i < count($resultat); $i++){
                    $n = $i + 1;
                    echo 'Utilisateur n°' .$n. ' :<br>';
                    foreach($resultat[$keys[$i]] as $key => $value){
                        echo $key. ' : ' .$value. '<br>';
                    }
                    echo '<br>';
                }
            }   
            catch(PDOException $e){
                echo 'Impossible de traiter les données. Erreur : '.$e->getMessage();
            }

          -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>